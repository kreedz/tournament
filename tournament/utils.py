def get_group_counts(participants_len):
    counts = []
    max_groups = 4
    for i in range(2, max_groups+1):
        div = participants_len / i
        if div < max_groups + 1:
            counts.append(div)
    return max(counts)


def get_list_of_list(lst, n):
    return [lst[i:i+n] for i in range(0, len(lst), n)]
