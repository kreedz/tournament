# -*- coding: utf-8 -*
from __future__ import unicode_literals

from django.db import models


class Tournament(models.Model):
    ROUND_ROBIN = 'Круговой турнир'
    PLAYOFF = 'Олимпийская система (только плей-офф)'
    ROUND_PLAYOFF = 'Круговой + плей-офф'
    STYLE_CHOICES = (
        (1, ROUND_ROBIN),
        (2, PLAYOFF),
        (3, ROUND_PLAYOFF),
    )

    name = models.CharField(verbose_name='Название', max_length=100)
    style = models.IntegerField(
        choices=STYLE_CHOICES, verbose_name='Тип турнира')
    is_subgroups = models.BooleanField(verbose_name='Подгруппы', default=False)
    winner = models.ForeignKey('Participant', null=True, blank=True)
    is_count_ps_playoff_eq_len_ps = models.BooleanField(
        verbose_name='Количество участников для плейофф равно общему количеству участников', default=False)

    class Meta:
        verbose_name = 'Турнир'
        verbose_name_plural = 'Турниры'

    def __unicode__(self):
        return '{0}, Тип: {1}'.format(self.name, self.get_style_display())


class Participant(models.Model):
    name = models.CharField(verbose_name='Название', max_length=100)

    class Meta:
        verbose_name = 'Участник'
        verbose_name_plural = 'Участники'

    def __unicode__(self):
        return '{0}'.format(self.name)


class Match(models.Model):
    A = 'A'
    B = 'B'
    C = 'C'
    D = 'D'
    GROUP_CHOICES = (
        (1, A),
        (2, B),
        (3, C),
        (4, D),
    )

    tournament = models.ForeignKey('Tournament')
    participant1 = models.ForeignKey(
        'Participant', verbose_name='Первый игрок',
        related_name='participant1', null=True
    )
    participant2 = models.ForeignKey(
        'Participant', verbose_name='Второй игрок',
        related_name='participant2', null=True
    )
    date = models.DateField(verbose_name='Дата', null=True, blank=True)
    result = models.CharField(
        verbose_name='Результат', max_length=10, null=True)
    winner = models.ForeignKey('Participant', null=True)
    prev_match1 = models.ForeignKey(
        'Match', related_name='match1', null=True, blank=True)
    prev_match2 = models.ForeignKey(
        'Match', related_name='match2', null=True, blank=True)
    level = models.SmallIntegerField(null=True, blank=True)
    group = models.SmallIntegerField(
        choices=GROUP_CHOICES, null=True, blank=True)

    class Meta:
        verbose_name = 'Матч'
        verbose_name_plural = 'Матчи'

    def __unicode__(self):
        return 'Турнир: {0}, дата: {1}'.format(self.tournament.name, self.date)


class Set(models.Model):
    match = models.ForeignKey('Match')
    set_number = models.SmallIntegerField(verbose_name='Номер сета')
    points1 = models.SmallIntegerField(verbose_name='Очки первого игрока')
    points2 = models.SmallIntegerField(verbose_name='Очки второго игрока')
