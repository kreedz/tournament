# -*- coding: utf-8 -*
from django import forms


class LoginForm(forms.Form):
    name = forms.CharField(label=u'Имя пользователя', required=True)
    password = forms.CharField(label=u'Пароль', required=True)
