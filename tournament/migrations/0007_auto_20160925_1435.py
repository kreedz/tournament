# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-25 14:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tournament', '0006_match_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='participant1',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='participant1', to='tournament.Participant'),
        ),
        migrations.AlterField(
            model_name='match',
            name='participant2',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='participant2', to='tournament.Participant'),
        ),
    ]
