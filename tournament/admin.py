from django.contrib import admin

from .models import Tournament, Participant, Match


class MatchAdmin(admin.ModelAdmin):
    exclude = ('prev_match1', 'prev_match2', 'level', 'group')


class TournamentAdmin(admin.ModelAdmin):
    exclude = ('is_subgroups',)


admin.site.register(Tournament, TournamentAdmin)
admin.site.register(Participant)
admin.site.register(Match, MatchAdmin)
