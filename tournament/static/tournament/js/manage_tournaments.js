(function(){
    var csrftoken = Cookies.get('csrftoken');
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    function initDatePicker() {
        var datepicker = $('.datepicker');
        if (!datepicker.length) {
            return;
        }
        $('.datepicker').datepicker({
            changeDay: true,
            changeMonth: true,
            changeYear: true,
            regional: 'ru',
            dateFormat: 'dd.mm.yy',
        });
    }

    $('.tournaments-list img').each(function(){
        $(this).on('click', function(){
            var tr = $(this).parents('tr');
            $.ajax({
                url: 'delete/' + tr.attr('data-id'),
                type: 'DELETE',
                success: function(){
                    tr.hide('slow', function(){
                        $(this).remove();
                    });
                }
            });
        });
    });

    var radios = $('form .radio');
    var styleName = {
        'ROUND_ROBIN': radios.find('[value="1"]').attr('data-name'),
        'PLAYOFF': radios.find('[value="2"]').attr('data-name'),
        'ROUND_PLAYOFF': radios.find('[value="3"]').attr('data-name'),
    }

    function isValid(participants, styleEl) {
        var n = participants.length;
        if (n < 2 || n > 32) {
            alert('Турниры поддерживают только от 2 до 32 участников!');
            return false;
        }
        if (styleEl.attr('data-name') == styleName.PLAYOFF) {
            if (!(n > 0 && (n & (n - 1))) == 0) {
                alert(styleEl.attr('data-name') + ' поддерживает только 2, 4, 8, 16, 32 участников!');
                return false;
            }
        }
        return true;
    }

    function getTextsFromSelect(select) {
        var selected = select.find(':selected');
        var texts = [];
        selected.each(function(){
            texts.push($(this).text());
        });
        return texts;
    }

    $('#participants-select').on('change', function(){
        var participants = getTextsFromSelect($(this));
        $('#text-tournament-participants').val(participants.join(','));
        $('#participants-count__value').text(participants.length);
    });

    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    var submitButton = $('button.save-data');
    $('form').submit(function(e){
        e.preventDefault();
        var name = $('#input-tournament').val();
        var styleEl = $('input[name="tournament-styles"]:checked');
        var style = styleEl.val();
        var styleName = styleEl.attr('data-name');
        var isSubgroups = $('#is-subgroups').is(':checked');
        var participantsSel = getTextsFromSelect($('#participants-select'));
        var participants = $('#text-tournament-participants').val().split(',').map(
            function(e){
                return e.trim();
            }
        ).filter(Boolean);
        participants = participants.filter(onlyUnique);
        if (!isValid(participants, styleEl)) {
            return;
        }
        submitButton.prop('disabled', true);
        $.ajax({
            url: 'create',
            type: 'POST',
            data: JSON.stringify({
                'name': name,
                'style': style,
                'style_name': styleName,
                'participants': participants,
                'is_subgroups': isSubgroups,
            }),
            contentType: 'application/json; charset=UTF-8',
            beforeSend: function(xhr, settings){
                $.ajaxSettings.beforeSend(xhr, settings);
                $('img.load-animate').show();
            }
        }).done(function(){
            location.href = '/';
        });
    });

    $('button[type="reset"]').click(function(e){
        location.href = '/';
    });

    $('input[name="tournament-styles"]').each(function(){
        $(this).on('click', function(){
            if ($(this).attr('data-name') == styleName.ROUND_PLAYOFF) {
                $('#is-subgroups').prop('disabled', false);
            } else {
                $('#is-subgroups').prop('checked', false);
                $('#is-subgroups').prop('disabled', true);
            }
        });
    });

    $('.schedule tbody td').each(function(){
        if (!$(this).text() && !$(this).hasClass('match-winner') && $('.superuser').length) {
            if ($(this).hasClass('match-additional-result') && $(this).siblings('.match-result:not(:has(input))').length) {
                return;
            }
            var input = $('<input/>').width('90px').appendTo($(this));
            if ($(this).hasClass('match-date')) {
                input.attr({placeholder: 'ДД.ММ.ГГГГ'});
                input.addClass('datepicker');
            }
            if ($(this).hasClass('match-result')) {
                input.attr({placeholder: 'X-X'});
            }
            if ($(this).hasClass('match-additional-result')) {
                input.attr({placeholder: 'X-X;X-X;'});
            }
        }
        initDatePicker();
    });

    function getSplittedResult(result) {
        var dash = result.indexOf('-');
        return [result.substring(0, dash), result.substring(dash+1, result.length)];
    }

    $(document).ready(function(){
        $('.save-data').prop('disabled', false);
    });

    var additionalResultMsg = 'Расширенная информация по счёту должна соответствовать шаблону X-X;X-X;, где X - любое число, количество X-X равно количеству сыгранных сетов/раундов в матче!';
    $('.schedule thead td.match-additional-result').prop('title', additionalResultMsg);

    $('#save-matches').on('click', function(){
        var matches = [];
        var schedule = $('.schedule:last');
        var trs = schedule.find('tbody tr');
        trs.each(function(){
            var tr = $(this);
            var resultEl = tr.find('td.match-result input');
            var result = '';
            if (resultEl.length) {
                result = resultEl.val();
            }
            var dateEl = tr.find('td.match-date input');
            var date = '';
            if (dateEl.length) {
                date = dateEl.val();
            }
            var additionalResult = '';
            var additionalResultEl = tr.find('td.match-additional-result input');
            if (additionalResultEl.length) {
                additionalResult = additionalResultEl.val();
            }
            if (!resultEl.length && !dateEl.length || !dateEl.length && !result || !resultEl.length && !date) {
                if (!additionalResultEl.length || !additionalResult) {
                    return;
                }
            }
            var tournamentName = $('.tournament-data').attr('data-tournament-name');
            var is_playoff_table = schedule.attr('data-is-playoff');
            var isDraw = false;
            if (result.length > 2) {
                var results = getSplittedResult(result);
                var isDraw = (is_playoff_table == 'True' && results[0] == results[1]);
            }
            var regexpResult = /^\d+?-\d+?$|^$/;
            var regexpDate = /^\d{1,2}\.\d{1,2}\.\d{4}$|^$/;
            var regexpAdditionalResult = /^(\d+-\d+;){1,}$|^$/;

            if (regexpDate.test(date) && regexpResult.test(result) && !isDraw && regexpAdditionalResult.test(additionalResult)) {
                var args = {
                    'id': tr.attr('data-match-id'),
                    'is_playoff_table': is_playoff_table,
                }
                if (date) {
                    args['date'] = date;
                }
                if (additionalResult) {
                    args['additional_result'] = additionalResult;
                }
                if (result) {
                    args['result'] = result;
                    args['additional_result'] = additionalResult;
                }
                matches.push(args);
            } else {
                var errMsg = '';
                if (isDraw) {
                    errMsg += 'Ничья запрещена в плей-офф!\r\n';
                }
                if (!regexpDate.test(date)) {
                    errMsg += 'Дата должна соответствовать шаблону ДД.ММ.ГГГГ!\r\n';
                }
                if (!regexpResult.test(result)) {
                    errMsg += 'Счёт должен соответствовать шаблону X-X, где X - любое число!\r\n';
                }
                if (!regexpAdditionalResult.test(additionalResult)) {
                    errMsg += additionalResultMsg + '\r\n';
                }
                alert(errMsg);
                return;
            }
        });
        if (matches.length) {
            submitButton.prop('disabled', true);
            $.ajax({
                url: '/matches/update/',
                type: 'POST',
                data: JSON.stringify(matches),
                contentType: 'application/json; charset=UTF-8',
                beforeSend: function(xhr, settings){
                    $.ajaxSettings.beforeSend(xhr, settings);
                    $('img.load-animate').show();
                }
            }).done(function(){
                location.reload();
            });
        }
    });

    function getEditableParentTable() {
        var tournamentScheduleName = '.tournament.schedule';
        return $(tournamentScheduleName + ' input').parents(tournamentScheduleName);
    }

    function getGamesCountInTable(table) {
        return table.find('tbody tr td.match-date input').length;
    }

    function getDateForStart() {
        var lastDate = $('td.match-date:not(:has(*)):last');
        if (lastDate.length) {
            var date = getDateObj(lastDate.text());
            date.setDate(date.getDate() + 1);
        } else {
            date = new Date();
        }
        return getDateString(getNextWorkingDate(date));
    }

    function addDateFill() {
        var parentTable = getEditableParentTable();
        var table = parentTable.find('table');
        var dateFill = $('<div>', {id: 'date-fill'}).insertBefore(parentTable);
        dateFill.hide();
        var expandFill = $('<div/>').insertBefore(dateFill);
        var a = $('<a/>', {text: 'Автоматически заполнить даты встреч', class: 'expand-fill'}).appendTo(expandFill);
        $('<span/>', {text: 'С'}).appendTo(dateFill);
        var startDate = $('<input/>', {
            class: 'datepicker start', placeholder: 'ДД.ММ.ГГГГ', value: getDateForStart(),
        }).appendTo(dateFill);
        $('<span/>', {text: 'Игр в день'}).appendTo(dateFill);
        var select = $('<select/>', {}).appendTo(dateFill);
        for (var i = 1; i <= getGamesCountInTable(table); ++i) {
            $('<option/>', {text: i, value: i}).appendTo(select);
        }
        var button = $('<button/>', {class: 'btn btn-default', text: 'Ок'}).appendTo(dateFill);
        button.on('click', function(){
            fillDates(table, select.val(), startDate);
        });
        a.on('click', function(){
            dateFill.show();
        });
        initDatePicker();
    }

    function getDateObj(date) {
        var date = /(\d+)\.(\d+)\.(\d+)/.exec(date);
        var year = date[3];
        var month = date[2];
        var day = date[1];
        return new Date(year + '/' + month + '/' + day);
    }

    function getDateString(date) {
        return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
    }

    function getNextWorkingDate(date) {
        if (date.getDay() == 0) {
            date.setDate(date.getDate() + 1);
        } else if (date.getDay() == 6) {
            date.setDate(date.getDate() + 2);
        }
        return date;
    }

    function fillDatesAlgorithm(trs, startDate, gamesPerDay) {
        var j = 0;
        startDate = getDateObj(startDate.val());
        for (var i = 0; i < trs.length; ++i) {
            if (j >= gamesPerDay) {
                startDate.setDate(startDate.getDate() + 1);
                j = 0;
            }
            startDate = getNextWorkingDate(startDate);
            ++j;
            $(trs[i]).find('.match-date input').val(getDateString(startDate));
        }
    }

    function getWeekendsCount(start, end) {
        count = 0;
        while (start.getTime() != end.getTime()) {
            if (start.getDay() == 0 || start.getDay() == 6) {
                ++count;
            }
            start.setDate(start.getDate() + 1);
        }
        return count;
    }

    function fillDates(table, gamesPerDay, startDate) {
        var inputs = table.find('tbody tr td.match-date input');
        var trs = inputs.parents('tr').get().reverse();
        if (gamesPerDay != 0) {
            fillDatesAlgorithm(trs, startDate, gamesPerDay);
        }
    }

    function randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    addDateFill();
})();
